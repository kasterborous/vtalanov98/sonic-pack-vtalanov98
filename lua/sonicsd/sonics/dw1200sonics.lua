SonicSD:AddSonic({
	ID="2nddoctorsonic",
	Name="2nd Doctor's Sonic",
	ViewModel="models/doctorwho1200/sonics/2nddoctor/1stpersonsonic.mdl",
	WorldModel="models/doctorwho1200/sonics/2nddoctor/3rdpersonsonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=2,
	DefaultLightColor = Color(5, 5, 5),
	SoundLoop = "doctorwho1200/sonics/2nddoctor/sonic.wav"
})

SonicSD:AddSonic({
	ID="3rddoctorsonic",
	Name="3rd Doctor's Sonic",
	ViewModel="models/doctorwho1200/sonics/3rddoctor/1stpersonsonic.mdl",
	WorldModel="models/doctorwho1200/sonics/3rddoctor/3rdpersonsonic.mdl",
	LightDisabled = true,
	LightPos=Vector(-100,0,0),
	LightBrightness=0,
	SoundLoop = "doctorwho1200/sonics/3rddoctor/sonic.wav"
})

SonicSD:AddSonic({
	ID="4thdoctorsonic",
	Name="4th Doctor's Sonic",
	ViewModel="models/doctorwho1200/sonics/4thdoctor/1stpersonsonic.mdl",
	WorldModel="models/doctorwho1200/sonics/4thdoctor/3rdpersonsonic.mdl",
	LightDisabled = true,
	LightPos=Vector(-100,0,0),
	LightBrightness=0,
    SoundLoop = "sonicsd/loop_1968_1.wav",
    SoundLoop2 = "sonicsd/loop_1968_2.wav",
})

SonicSD:AddSonic({
	ID="5thdoctorsonic",
	Name="5th Doctor's Sonic",
	ViewModel="models/doctorwho1200/sonics/5thdoctor/1stpersonsonic.mdl",
	WorldModel="models/doctorwho1200/sonics/5thdoctor/3rdpersonsonic.mdl",
	LightDisabled = true,
	LightPos=Vector(-100,0,0),
	LightBrightness=0,
    SoundLoop = "sonicsd/loop_1968_1.wav",
    SoundLoop2 = "sonicsd/loop_1968_2.wav",
})

SonicSD:AddSonic({
	ID="8thdoctorsonic",
	Name="8th Doctor's Sonic",
	ViewModel="models/doctorwho1200/sonics/8thdoctor/1stpersonsonic.mdl",
	WorldModel="models/doctorwho1200/sonics/8thdoctor/3rdpersonsonic.mdl",
	LightDisabled = true,
	LightPos=Vector(-100,0,0),
	LightBrightness=0,
	SoundLoop = "doctorwho1200/sonics/8thdoctor/sonic.wav",
    SoundLoop2 = "poogie/8_loop_2.wav",
})

SonicSD:AddSonic({
	ID="wardoctorsonic",
	Name="War Doctor's Sonic",
	ViewModel="models/doctorwho1200/sonics/wardoctor/1stpersonsonic.mdl",
	WorldModel="models/doctorwho1200/sonics/wardoctor/3rdpersonsonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=2,
	DefaultLightColor = Color(255, 0, 30),
	SoundLoop = "poogie/war_loop.wav",
    SoundLoop2 = "poogie/war_loop.wav",
    ButtonSoundOn = "poogie/war_button_on.wav",
    ButtonSoundOff = "poogie/war_button_off.wav",
})

SonicSD:AddSonic({
	ID="10thdoctorsonic",
	Name="10th Doctor's Sonic",
	ViewModel="models/doctorwho1200/sonics/10thdoctor/1stpersonsonic.mdl",
	WorldModel="models/doctorwho1200/sonics/10thdoctor/3rdpersonsonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=3,
	DefaultLightColor = Color(0, 0, 200),
	SoundLoop = "sonicsd/loop_2005_1.wav",
    SoundLoop2 = "sonicsd/loop_2005_2.wav",
    ButtonSoundOff = "sonicsd/button_off_2.wav",
})

SonicSD:AddSonic({
	ID="11thdoctorsonic",
	Name="11th Doctor's Sonic",
	ViewModel="models/doctorwho1200/sonics/11thdoctor/1stpersonsonic.mdl",
	WorldModel="models/doctorwho1200/sonics/11thdoctor/3rdpersonsonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=3,
    SoundLoop = "sonicsd/loop_2010_1.wav",
    SoundLoop2 = "sonicsd/loop_2010_2.wav",
	DefaultLightColorOff = Color(10, 100, 50),
    DefaultLightColor = Color(50, 250, 50),
    DefaultLightColor2 = Color(0, 200, 100)
})

SonicSD:AddSonic({
	ID="12thdoctorsonic",
	Name="12th Doctor's Sonic",
	ViewModel="models/doctorwho1200/sonics/12thdoctor/1stpersonsonic.mdl",
	WorldModel="models/doctorwho1200/sonics/12thdoctor/3rdpersonsonic.mdl",
	LightPos=Vector(-100,0,0),
	LightBrightness=3,
	DefaultLightColorOff = Color(0, 0, 0, 10),
    DefaultLightColor = Color(0, 0, 200),
    DefaultLightColor2 = Color(0, 250, 60),
    SoundLoop = "poogie/12_loop_2.wav",
	SoundLoop2 = "poogie/12_loop_1.wav",
	ButtonSoundOn = "poogie/12_button_on.wav",
    ButtonSoundOff = "poogie/12_button_off.wav",
	ButtonDelay = 0.16,
})